## S3 bucket for each static webiste
resource "aws_s3_bucket" "S3" {
  bucket = var.FQDN
  acl    = "private"
  website {
    index_document = var.index_document
    error_document = var.error_document
  }
  versioning {
    enabled = var.versioning_enabled
  }

  tags = local.service_tags
}
resource "aws_s3_bucket_policy" "S3" {
  bucket = aws_s3_bucket.S3.id
  policy = <<EOF
{
  "Version": "2008-10-17",
  "Id": "Policyforhosting",
  "Statement": [
    {
     "Sid": "1",
     "Effect": "Allow",
     "Principal": "*",
     "Action": "s3:GetObject",
     "Resource": "arn:aws:s3:::${var.FQDN}/*" 
    }
  ]
}
EOF
}
